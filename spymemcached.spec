Name:                spymemcached
Version:             2.11.4
Release:             1
Summary:             Java client for memcached
License:             ASL 2.0 and MIT
Url:                 https://github.com/dustin/java-memcached-client
Source0:             https://github.com/dustin/java-memcached-client/archive/%{version}.tar.gz
BuildRequires:       maven-local mvn(com.codahale.metrics:metrics-core) mvn(log4j:log4j:1.2.17)
BuildRequires:       mvn(org.slf4j:slf4j-api) mvn(org.springframework:spring-beans)
%if 0
BuildRequires:       mvn(jmock:jmock) >= 1.2.0 mvn(junit:junit)
%endif
Requires:            mvn(log4j:log4j:1.2.17)
BuildArch:           noarch
%description
A simple, asynchronous, single-threaded memcached client written in java.

%package javadoc
Summary:             Javadoc for spymemcached
%description javadoc
This package contains javadoc for spymemcached.

%prep
%setup -q -n java-memcached-client-%{version}
find -name '*.jar' -delete
find -name '*.class' -delete
sed -i "s|2.999.999-SNAPSHOT|%{version}|" pom.xml
sed -i.log4j12 "s|<version>1.2.16|<version>1.2.17|" pom.xml
native2ascii -encoding UTF-8 src/main/java/net/spy/memcached/MemcachedConnection.java \
 src/main/java/net/spy/memcached/MemcachedConnection.java
%pom_remove_dep :jmock
%mvn_file :spymemcached spymemcached
%mvn_alias :spymemcached spy:spymemcached spy:memcached

%build
%mvn_build -f -- -Dproject.build.sourceEncoding=UTF-8

%install
%mvn_install

%files -f .mfiles
%doc README.markdown
%license LICENSE.txt

%files javadoc -f .mfiles-javadoc
%license LICENSE.txt

%changelog
* Fri Aug 14 2020 yaokai <yaokai13@huawei.com> - 2.11.4-1
- Package init
